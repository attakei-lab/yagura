# Yagura on Firebase

Web request monitoring platform.

## Description

This project provides "Website monitoring platform" on Firebase.
This include these features.

* Control web-application by Firebase Hosting.
* Monitoring backend by Cloud Functions.

## Requirements

Firebase must be using paid plan, because it execute HTTP request to outside website.

And you need Node.js environment for local development.

If you want to know more information, please check subpackage in `packages/devel`.

## Usage

Not yet (will update after implement deploy to firebase)

## Code policies

This project includes sub-packages.

### @attakei/yagura

Manage Firebase backend components and refered logic and types from client.
This includes util functions and types.

### @attakei/yagura-app

Manage web-application for user and admin, in [packages/app](./packages/app) folder.

### @attakei/yagura-devel

Manage common development dependencies to lock versions in [packages/devel](./packages/devel) folder.

## License

Apache License Version 2.0. See [it](./LICENSE)
