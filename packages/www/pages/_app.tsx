import App from 'next/app';
import * as React from 'react';
import { Provider } from 'react-redux';
import { bugsnagConfig } from '@attakei/yagura/lib/config';
import WithBugsnag from '~/components/wrappers/WithBugsnag';
import BaseLayout from '~/layouts/BaseLayout';
import MainLayout from '~/layouts/MainLayout';
import FirebaseProvider from '~/services/auth/provider';
import { getFirebase } from '~/services/firebase';
import { setupStore } from '~/store';

export class YaguraApp extends App {
  render() {
    const store = setupStore();
    const firebaseApp = getFirebase();
    const { Component, pageProps } = this.props;
    return (
      <WithBugsnag bugsnagConfig={bugsnagConfig}>
        <Provider store={store}>
          <FirebaseProvider firebaseApp={firebaseApp}>
            <BaseLayout>
              <MainLayout>
                <Component {...pageProps} />
              </MainLayout>
            </BaseLayout>
          </FirebaseProvider>
        </Provider>
      </WithBugsnag>
    );
  }
}

export default YaguraApp;
