import React, { ReactElement } from 'react';
import { Container, Hero } from 'react-bulma-components';

export const render = (): ReactElement => {
  return (
    <Hero size='large' textWeight='bold' color='primary'>
      <Hero.Body>
        <Container>
          <h1 className='title'>Welcome to Yagura!</h1>
          <h2 className='subtitle'>
            Yagura provides simple website monitoring platform.
          </h2>
        </Container>
      </Hero.Body>
    </Hero>
  );
};

export default render;
