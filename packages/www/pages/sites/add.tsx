/***************************************
 * Page of site-adding form for logged-in users.
 */
import React from 'react';
import { Container, Section } from 'react-bulma-components';
import PageTitle from '~/components/PageTitle';


export const render: React.FC = () => {
  return (
    <>
      <PageTitle title='Adding website' />
      <Section>
        <Container></Container>
      </Section>
    </>
  );
};

export default render;
