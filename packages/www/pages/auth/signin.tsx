import { useRouter } from 'next/router';
import React, { FormEvent, ReactElement, useState } from 'react';
import {
  Button,
  Columns,
  Container,
  Content,
  Hero,
  Message,
  Section,
} from 'react-bulma-components';

import firebase, { getFirebase } from '~/services/firebase';

interface MessageData {
  body: string;
  color: 'info' | 'danger' | 'success';
}

export const render = (): ReactElement => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState<MessageData>({
    body: 'Please push "sign in/up" button',
    color: 'info',
  });

  /**
   * Procede authencitation by Firebase
   *
   * @param e Form action event
   */
  const handleAuth = async (e: FormEvent): Promise<void> => {
    e.preventDefault();
    const auth = getFirebase().auth();
    const provider = new firebase.auth.GoogleAuthProvider();
    setMessage({ body: 'Processing...', color: 'info' });
    setLoading(true);
    await auth
      .signInWithPopup(provider)
      .then(uc => {
        setMessage({
          body: `Hello ${uc.user.displayName}. Please wait initialzing...`,
          color: 'success',
        });
        if (uc.additionalUserInfo.isNewUser) {
          const func = getFirebase()
            .functions()
            .httpsCallable('CONFIGURE_USER');
          return func({ email: uc.user.email, name: uc.user.displayName });
        }
      })
      .then(() => {
        setMessage({
          body: 'OK',
          color: 'success',
        });
        router.push('/dashboard');
      })
      .catch(err => {
        console.error(err);
        setMessage({ body: err.message || 'Error occured', color: 'danger' });
        setLoading(false);
      });
  };

  return (
    <>
      <Hero size='small'>
        <Hero.Body>
          <Container>
            <h1 className='title'>Sign in/Sign up</h1>
          </Container>
        </Hero.Body>
      </Hero>
      <Section>
        <Container>
          <Columns>
            <Columns.Column>
              <Message color={message.color} size='small'>
                <Message.Body>{message.body}</Message.Body>
              </Message>
            </Columns.Column>
          </Columns>
          <Columns>
            <Columns.Column>
              <Content>
                <p>Currently, Yagura can sign in/up only by Google Account.</p>
              </Content>
            </Columns.Column>
            <Columns.Column>
              <form onSubmit={handleAuth}>
                <Button
                  color='primary'
                  size='medium'
                  fullwidth
                  loading={loading}
                  onClick={handleAuth}
                >
                  Sign in/up by Google
                </Button>
              </form>
            </Columns.Column>
          </Columns>
        </Container>
      </Section>
    </>
  );
};

export default render;
