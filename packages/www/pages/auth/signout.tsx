import { useRouter } from 'next/router';
import React, { ReactElement, useEffect } from 'react';
import { Container, Hero } from 'react-bulma-components';

import { getFirebase } from '~/services/firebase';

export const render = (): ReactElement => {
  const router = useRouter();

  const handleSignOut = async (): Promise<void> => {
    await getFirebase()
      .auth()
      .signOut()
      .then(() => {
        router.push('/');
      });
  };

  useEffect(() => {
    setTimeout(handleSignOut, 1000);
  });

  return (
    <>
      <Hero size='small'>
        <Hero.Body>
          <Container>
            <h1 className='title'>Sign out now...</h1>
          </Container>
        </Hero.Body>
      </Hero>
    </>
  );
};

export default render;
