import * as sites from '@attakei/yagura/lib/sites';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Container,
  Content,
  Hero,
  Section,
  Table,
} from 'react-bulma-components';
import { useCollectionOnce } from 'react-firebase-hooks/firestore';
import { useDispatch, useSelector } from 'react-redux';

import { getFirebase } from '~/services/firebase';
import { StoreState } from '~/store';
import adminModule from '~/store/modules/adminModule';

export const render: React.FC = () => {
  const [inHandle, setInHandle] = useState(false);
  const query = getFirebase()
    .firestore()
    .collection(sites.SITE_COLLECTION);
  const [sitesSnapshot, initializing, error] = useCollectionOnce(query);
  const dispatch = useDispatch();
  useEffect(() => {
    if (!sitesSnapshot) return;
    const siteSet = {};
    sitesSnapshot.docs.forEach(ds => {
      siteSet[ds.id] = ds.data() as sites.SiteData;
    });
    dispatch(adminModule.actions.bindSites(siteSet));
  }, [sitesSnapshot]);
  const siteSet = useSelector<StoreState, { [key: string]: sites.SiteData }>(
    state => state.admin.sites,
  );
  const handleDelete = (siteId: string): (() => Promise<void>) => {
    return async (): Promise<void> => {
      setInHandle(true);
      const confirmed = confirm('Delete it?');
      if (!confirmed) {
        setInHandle(false);
        return;
      }
      const callable = getFirebase()
        .functions()
        .httpsCallable('DELETE_SITE');
      await callable({ siteId })
        .then(() => {
          console.log('Deleted');
          dispatch(adminModule.actions.deleteSite(siteId));
        })
        .catch(err => {
          console.error(err);
        })
        .finally(() => {
          setInHandle(false);
        });
    };
  };
  return (
    <>
      <Hero size='small'>
        <Hero.Body>
          <Container>
            <h1 className='title'>Site management</h1>
          </Container>
        </Hero.Body>
      </Hero>
      <Section>
        <Container>
          <Content>
            {(() => {
              if (initializing) {
                return <p>Initializing...</p>;
              }
              if (error) {
                return <p>{error.message}</p>;
              }
              return (
                <Table>
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>URL</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {Object.entries(siteSet).map(([siteId, siteData]) => {
                      return (
                        <tr key={siteId}>
                          <td>{siteData.name}</td>
                          <td>{siteData.url}</td>
                          <td>
                            <Button.Group hasAddons>
                              <Button
                                size='small'
                                color='danger'
                                onClick={handleDelete(siteId)}
                                disabled={inHandle}
                              >
                                Delete
                              </Button>
                            </Button.Group>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              );
            })()}
          </Content>
        </Container>
      </Section>
    </>
  );
};

export default render;
