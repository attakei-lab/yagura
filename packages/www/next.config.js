const path = require('path')
const withSass = require('@zeit/next-sass')


module.exports = withSass({
  webpack(config, options) {
    config.resolve.alias = {
      ...config.resolve.alias,
      "~": path.resolve('src')
    }
    return config
  }
})
