import { createSlice, Actions, CaseReducers } from 'redux-starter-kit';
import { Action, Reducer } from 'redux';
import { User } from '~/services/auth/types';

export type State = {
  initializing: boolean;
  error?: Error;
  user?: User;
}

export const configure: Reducer<State, Action & { payload: State }> = (state, action) => {
  return {
    ...state,
    ...action.payload,
  }
}

const adminModule = createSlice<State, CaseReducers<State, Actions>>({
  name: 'auth',
  initialState: {
    initializing: true,
  },
  reducers: {
    configure,
  },
});

export default adminModule;
