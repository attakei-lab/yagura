import * as sites from '@attakei/yagura/lib/sites';
import { createSlice } from 'redux-starter-kit';
import { Action, Reducer } from 'redux';

type SiteDataSet = {
  [key: string]: sites.SiteData;
};

export interface State {
  sites: SiteDataSet;
}

export const bindSites: Reducer<State, Action & { payload: SiteDataSet }> = (
  state,
  action,
) => {
  return {
    ...state,
    sites: action.payload,
  };
};

export const deleteSite: Reducer<State, Action & { payload: string }> = (
  state,
  action,
) => {
  const sites: SiteDataSet = {};
  Object.entries(state.sites).forEach(([siteId, siteData]) => {
    if (siteId !== action.payload) {
      sites[siteId] = siteData;
    }
  });
  return {
    ...state,
    sites,
  };
};

const adminModule = createSlice({
  name: 'admin',
  initialState: {
    sites: {},
  },
  reducers: {
    bindSites,
    deleteSite,
  },
});

export default adminModule;
