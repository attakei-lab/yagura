import {
  getDefaultMiddleware,
  configureStore,
  combineReducers,
} from 'redux-starter-kit';
import adminModule, { State as AdminState } from './modules/adminModule';
import authModule, { State as AuthState } from './modules/authModule';

export interface StoreState {
  admin: AdminState;
  auth: AuthState;
}

const rootReducer = combineReducers({
  admin: adminModule.reducer,
  auth: authModule.reducer,
});

export const setupStore = () => {
  const middlewares = [...getDefaultMiddleware()];

  const store = configureStore({
    reducer: rootReducer,
    middleware: middlewares,
  });
  return store;
};
