/*******************
 * Wrapper to handle Firebase cliend in @attakei/yagura-app
 */
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';
import { firebaseConfig } from '@attakei/yagura/lib/config';

/**
 * Return singleton Firebase client application.
 * If not exists, initialize first.
 */
export const getFirebase = (): firebase.app.App => {
  // Initialize
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }
  return firebase.app();
};

export default firebase;
