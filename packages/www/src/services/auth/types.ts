import * as auth from '@attakei/yagura/lib/auth';
import { User as FirebaseUser } from 'firebase';

export interface User {
  authUser: FirebaseUser;
  userProfile: auth.UserProfile;
}
