/***************************************
 * User binding Firebase user
 */
import { Error as AuthError } from '@firebase/auth-types';
import * as auth from '@attakei/yagura/lib/auth';
import { app, User as AuthUser } from 'firebase';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useDocumentDataOnce } from 'react-firebase-hooks/firestore';

import { User } from './types';

interface HookedError {
  err: AuthError | Error;
}

/**
 * Call Firebase Authentication and Firestore and return binding object.
 *
 * @param app Firebase client app
 */
export const useUserWithProfile = (
  firebase: app.App,
): [User | undefined, boolean, HookedError | null] => {
  // Fetching data
  const [authUser, authInitializing, authError] = useAuthState(firebase.auth());
  const profileRef = firebase
    .firestore()
    .collection(auth.USER_PROFILE_COLLECTION)
    .doc(authUser ? authUser.uid : 'anonymous');
  const [userProfile, profileInitializing, profileError] = useDocumentDataOnce(
    profileRef,
  );
  if (authInitializing) {
    return [undefined, true, null];
  }
  if (authError) {
    return [undefined, false, { err: authError }];
  }
  if (profileInitializing) {
    return [undefined, true, null];
  }
  if (profileError) {
    return [undefined, false, { err: profileError }];
  }
  if (!authUser || !userProfile) {
    return [undefined, false, null];
  }
  return [{ authUser: authUser.toJSON() as AuthUser, userProfile }, false, null];
};
