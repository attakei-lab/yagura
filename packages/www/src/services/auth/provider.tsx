/***************************************
 * Firebase authentication provider
 */
import { app } from 'firebase';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useUserWithProfile } from '~/services/auth/use-user-with-profile';

type Props = {
  firebaseApp: app.App;
}

export const render: React.FC<Props> = ({ children, firebaseApp }) => {
  const dispatch = useDispatch();
  const [user, initializing, error] = useUserWithProfile(firebaseApp);
  dispatch({type: 'auth/configure', payload: {initializing, error, user}});
  return (
    <>
      {children}
    </>
  );
};

export default render;
