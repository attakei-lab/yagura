/***************************************
 * Configuration variables
 */

/**
 * Firebase config from https://console.firebase.google.com/
 */
// export const firebaseConfig = {
//   apiKey: "",
//   authDomain: "",
//   databaseURL: "",
//   projectId: "",
//   storageBucket: "",
//   appId: ""
// };

/**
 * Bugsnag config from https://app.bugsnag.com/
 */
export const bugsnagConfig = {
  apiKey: '',
};
