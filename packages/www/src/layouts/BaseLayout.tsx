/***************************************
 */
import '~/styles/default.scss';

import { useRouter } from 'next/router';
import React from 'react';
import { useSelector } from 'react-redux';
import Error from '~/components/Error';
import Initializing from '~/components/Initializing';
import { StoreState } from '~/store';
import { State as AuthState } from '~/store/modules/authModule';

const openPages = ['/', '/auth/signin'];

export const render: React.FC = ({ children }) => {
  const router = useRouter();
  const {user, initializing, error}: AuthState = useSelector<StoreState, AuthState>(state => state.auth);
  let child: React.ReactNode | null = null;
  if (initializing) {
    child = <Initializing />;
  } else if (error) {
    child = <Error />;
  } else if (user) {
    child = children;
  } else if (openPages.includes(router.pathname)) {
    child = children;
  } else {
    router.push('/auth/signin');
  }
  return (
    <>
      {child}
    </>
  );
}

export default render;
