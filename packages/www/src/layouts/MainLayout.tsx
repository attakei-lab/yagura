import React from 'react';
import { useSelector } from 'react-redux';
import { StoreState } from '~/store';
import { State as AuthState } from '~/store/modules/authModule';
import Navbar from '~/components/Navbar';

export const render: React.FC = ({ children }) => {
  const { user }: AuthState = useSelector<StoreState, AuthState>(state => state.auth);
  return (
    <>
      <Navbar user={user} />
      {children}
    </>
  );
};

export default render;
