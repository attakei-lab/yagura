import 'jest';

import { shallow } from 'enzyme';
import * as React from 'react';

import * as t from './PageTitle';

describe('PageTitlte.render()', () => {
  it('render title', () => {
    const component = shallow(<t.render title='Example' />);
    expect(component.contains('Example')).toBeTruthy();
  });

  describe('For subtitle', () => {
    it('defined', () => {
      const component = shallow(<t.render title='Example' subtitle='Sub-example' />);
      expect(component.contains('Sub-example')).toBeTruthy();
    });  

    it('undefined', () => {
      const component = shallow(<t.render title='Example' />);
      expect(component.contains('</h2>')).toBeFalsy();
    });  
  });
});
