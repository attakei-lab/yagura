/**
 * Navigation bar components
 */
import React from 'react';
import { Container, Hero, HeroProps } from 'react-bulma-components';

interface Props extends HeroProps {
  title: string;
  subtitle?: string;
}

export const render: React.FC<Props> = (
  { color, size, title, subtitle }
) => {
  return (
    <Hero size={size || 'small'} color={color}>
      <Hero.Body>
        <Container>
          <h1 className='title'>{title}</h1>
          {subtitle ? <h2 className='subtitle'>{subtitle}</h2> : null}
        </Container>
      </Hero.Body>
    </Hero>
  );
};

export default render;
