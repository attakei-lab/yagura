/**
 * Navigation bar components
 */
import '~/styles/default.scss';

import Link from 'next/link';
import React, { ReactElement, useState, useEffect } from 'react';
import { Navbar } from 'react-bulma-components';

import { User } from '~/services/auth/types';
import { useRouter } from 'next/router';

interface Props {
  user?: User;
}

export const render = (props: Props): ReactElement => {
  const router = useRouter();
  const [active, setActive] = useState(false);
  const toggleActive = (): void => setActive(!active);

  useEffect(() => {
    setActive(false);
  }, [router]);

  return (
    <Navbar color='primary' active={active}>
      <Navbar.Brand>
        <Link href='/'>
          <a className='navbar-item'>Yagura</a>
        </Link>
        <Navbar.Burger onClick={toggleActive} />
      </Navbar.Brand>
      <Navbar.Menu>
        <Navbar.Container position='start'>
          { props.user
            ?
            <>
              <Link href='/sites/add'>
                <a className='navbar-item'>Add website</a>
              </Link>
            </>
            :
            null
          }
          {((): React.ReactNode => {
            if (!props.user) {
              return null;
            }
            if (!props.user.userProfile.isAdmin) {
              return null;
            }
            return (
              <Navbar.Item dropdown hoverable>
                <Navbar.Link>admin</Navbar.Link>
                <Navbar.Dropdown>
                  <Link href='/admin/sites'>
                    <a className='navbar-item'>Sites</a>
                  </Link>
                </Navbar.Dropdown>
              </Navbar.Item>
            );
          })()}
        </Navbar.Container>
        <Navbar.Container position='end'>
          {!props.user ? (
            <Link href='/auth/signin'>
              <a className='navbar-item'>Sign in/up</a>
            </Link>
          ) : (
            <Navbar.Item dropdown hoverable>
              <Navbar.Link>{props.user.userProfile.name}</Navbar.Link>
              <Navbar.Dropdown>
                <Link href='/auth/signout'>
                  <a className='navbar-item'>Sign out</a>
                </Link>
              </Navbar.Dropdown>
            </Navbar.Item>
          )}
        </Navbar.Container>
      </Navbar.Menu>
    </Navbar>
  );
};

export default render;
