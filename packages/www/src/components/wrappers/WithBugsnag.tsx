/***************************************
 * Bugsnag wrapper for my React app
 */
import * as React from 'react';
import bugsnag, { Bugsnag } from '@bugsnag/js';
import bugsnagReact from '@bugsnag/plugin-react';

type Props = {
  bugsnagConfig?: Bugsnag.IConfig;
}

export const render: React.FC<Props> = ({ bugsnagConfig, children }) => {
  if (!bugsnagConfig) {
    console.debug('Not specified apiKey');
    return (<>{children}</>);
  }
  const bugsnagClient = bugsnag(bugsnagConfig);
  bugsnagClient.use(bugsnagReact, React);
  const ErrorBoundary = bugsnagClient.getPlugin('react');
  return (<ErrorBoundary>{children}</ErrorBoundary>);
}

export default render;
