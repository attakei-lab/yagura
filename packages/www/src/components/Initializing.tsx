import React, { ReactElement } from 'react';
import { Container, Hero } from 'react-bulma-components';

export const render = (): ReactElement => {
  return (
    <Hero size='small'>
      <Hero.Body>
        <Container>
          <h1>Initializing ...</h1>
        </Container>
      </Hero.Body>
    </Hero>
  );
};

export default render;
