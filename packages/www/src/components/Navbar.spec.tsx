import 'jest';

import { shallow } from 'enzyme';
import * as React from 'react';

import { User } from '~/services/auth/types';

import * as t from './Navbar';

describe('Navbar.render()', () => {
  it('not authenticated', () => {
    const navbar = shallow(<t.render />);
    expect(navbar.contains('Sign in/up')).toBeTruthy();
    expect(navbar.contains('Sign out')).toBeFalsy();
  });

  it('authenticated', () => {
    const user: User = {
      authUser: null, // Mock
      userProfile: {
        email: 'user@example.com',
        name: 'Test user',
        isAdmin: false,
      },
    };
    const navbar = shallow(<t.render user={user} />);
    expect(navbar.contains(user.userProfile.name)).toBeTruthy();
    expect(navbar.contains('Sign out')).toBeTruthy();
    expect(navbar.contains('Sign in/up')).toBeFalsy();
  });
});
