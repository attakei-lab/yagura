import * as path from 'path';
import * as targz from 'targz';
import * as yargs from 'yargs';

type CallbackError = string | Error | null;

const PROJECT_ROOT = path.join(__dirname, '..', '..');

const makeIgnoreRule = (
  target: string,
  ignores: string[],
): ((path: string) => boolean) => {
  return (path: string): boolean => {
    if (!path.startsWith(target)) return true;
    const basename = path.replace(target, '').slice(1);
    return ignores.filter(ignore => basename.startsWith(ignore)).length > 0;
  };
};

const makeCompressOptions = (src: string, dest: string): targz.options => {
  const arcCwd = path.join(src, '..');
  return {
    src: arcCwd,
    dest,
    tar: {
      ignore: makeIgnoreRule(src, ['node_modules']),
    },
  };
};

const compressCallback = (err: CallbackError): void => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
};

yargs.command(
  '* <src> <dest>',
  'Make archive of package',
  () => {},
  (args: any) => {
    const src = path.join(PROJECT_ROOT, args.src);
    const dest = path.join(PROJECT_ROOT, args.dest);
    targz.compress(makeCompressOptions(src, dest), compressCallback);
  },
).argv;
