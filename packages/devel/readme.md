# @attakei/yagura-devel

Development packages for Yagura.

## Description

This sub-package manage these.

* Common development dependencies for other sub-packages.
* Integration tools on multiple sub-packages.


## Usage

Other sub-packages need depend on this as `peerDependencies`.

```json
{
    "peerDependencies": {
        "@attakei/yagura-devel": "0.1.0"
    }
}
```
