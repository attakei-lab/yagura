/***************************************
 * Clean up backend anything
 */
import { WriteResult } from '@google-cloud/firestore';
import * as admin from 'firebase-admin';

import * as auth from '../auth';

export const command = 'cleanup-backend';

export const describe = 'Clean up backend data (auth, firestore)';

export const builder = {};

/**
 * Main handler
 *
 * @param argv Target information
 */
export const handler = async (): Promise<void> => {
  console.log('Start cleaning...');
  admin.initializeApp();
  // Set anonymous profile
  await admin
    .firestore()
    .doc(`${auth.USER_PROFILE_COLLECTION}/${auth.ANONYMOUS_UID}`)
    .set(auth.ANONYMOUS_PROFILE);
  // Clean up users
  const userProfiles = await admin
    .firestore()
    .collection(auth.USER_PROFILE_COLLECTION)
    .get()
    .then(qs => qs.docs)
    .catch(err => {
      console.error(err);
      throw err;
    });
  const promises: Promise<void | WriteResult>[] = [];
  userProfiles
    .filter(up => up.id !== auth.ANONYMOUS_UID)
    .forEach(up => {
      promises.push(admin.auth().deleteUser(up.id));
      promises.push(up.ref.delete());
    });
  await Promise.all(promises);
};
