/***************************************
 * Monitor one website
 */
import { Arguments } from 'yargs';

import { getApp } from '../firebase';
import { Site, SiteMonitor } from '../monitors/services/siteMonitor';

export type MonitoringOrder = {
  siteId: string;
};

export const command = 'monitor-site <siteId>';

export const describe = 'Monitor website';

export const builder = {};

/**
 * Main handler
 *
 * @param argv Target information
 */
export const handler = async (
  argv: Arguments<MonitoringOrder>,
): Promise<void> => {
  console.log(`Try monitoring id:${argv.siteId}`);
  const firestore = getApp().firestore();
  const siteRef = firestore.collection('sites').doc(argv.siteId);
  const site = await siteRef.get().then(ds => {
    if (!ds.exists) {
      return null;
    }
    return ds.data() as Site;
  });
  if (site === null) {
    console.warn('Site is not exits in DB.');
    return;
  }
  const monitor = new SiteMonitor(site);
  const resp = await monitor.request();
  console.log(`Monitor date ${monitor.startAt}`);
  console.log(`HTTP Status: ${resp.statusCode}`);
  const reportRef = siteRef
    .collection('reports')
    .doc(monitor.startAt.unix().toString());
  await reportRef.set({
    monitorAt: monitor.startAt.unix(),
    httpStatus: {
      state: resp.statusCode && resp.statusCode === 200 ? 'OK' : 'NG',
      excepted: 200,
      actually: resp.statusCode,
      message: '',
    },
  });
};
