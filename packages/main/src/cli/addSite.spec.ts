import * as admin from 'firebase-admin';
import * as yargsParser from 'yargs-parser';

import * as sites from '../sites';
import * as command from './addSite';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const MockFirebase = require('mock-cloud-firestore');

jest.spyOn(console, 'warn').mockImplementation(x => x);
jest.spyOn(console, 'log').mockImplementation(x => x);
jest.spyOn(console, 'debug').mockImplementation(x => x);

const spyAdminInitializeApp = jest.spyOn(admin, 'initializeApp');
const spyAdminApp = jest.spyOn(admin, 'app');

describe('handler()', () => {
  let mockFirebase: admin.app.App;

  beforeEach(() => {
    spyAdminInitializeApp.mockImplementation(() => mockFirebase);
    spyAdminApp.mockImplementation(() => mockFirebase);
  });

  it('site is registerd', async done => {
    // Mocking
    mockFirebase = new MockFirebase();
    // Testing
    const argv = {
      name: 'EXAMPLE.COM',
      url: 'http://example.com',
      ...yargsParser(''),
    };
    await command.handler(argv);
    expect(console.log).toBeCalled();
    expect(console.warn).not.toBeCalled();
    await admin
      .app()
      .firestore()
      .collection(sites.SITE_COLLECTION)
      .get()
      .then(qs => {
        expect(qs.empty).toBeFalsy();
        done();
      });
  });

  it('site is already exists', async done => {
    // Mocking
    mockFirebase = new MockFirebase({
      __collection__: {
        sites: {
          __doc__: {
            xxxxx: {
              name: 'example.com',
              url: 'http://example.com',
            },
          },
        },
      },
    });
    // Testing
    const argv = {
      name: 'EXAMPLE.COM',
      url: 'http://example.com',
      ...yargsParser(''),
    };
    await command.handler(argv);
    expect(console.log).toBeCalled();
    expect(console.warn).toBeCalled();
    done();
  });
});
