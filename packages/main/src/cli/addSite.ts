/***************************************
 * Add monitoring target website into Firestore
 */
import * as dayjs from 'dayjs';
import * as admin from 'firebase-admin';
import { Arguments } from 'yargs';

import * as sites from '../sites';

export const command = 'add-site <url> <name>';

export const describe = 'Add monitring target website';

export const builder = {};

/**
 * Main handler
 *
 * @param argv Target information
 */
export const handler = async (
  argv: Arguments<sites.SiteData>,
): Promise<void> => {
  // Build site object
  const site: sites.SiteData = {
    name: argv.name,
    url: argv.url,
  };
  console.debug(site);
  // Check that site is already exists (by URL)
  admin.initializeApp();
  const collection = admin
    .app()
    .firestore()
    .collection('sites');
  const siteExists = await collection
    .where('url', '==', site.url)
    .get()
    .then(qs => !qs.empty);
  if (siteExists) {
    console.warn('Site is already exists');
  }
  // Register site
  const siteDocRef = collection.doc();
  await siteDocRef
    .set(site)
    .then((wr?) => {
      console.log(
        'Register site' +
          (wr ? ` at ${dayjs(wr.writeTime.toDate()).toISOString()}` : ''),
      );
    })
    .catch(err => {
      console.error(err);
    });
};
