import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

export type CallableFunction<D, R> = (
  data: D,
  context: functions.https.CallableContext,
) => Promise<R>;

export interface CallableModule<D, R> {
  handler: CallableFunction<D, R>;
}

export const getApp = (): admin.app.App => {
  if (admin.apps.length <= 1) {
    admin.initializeApp();
  }
  return admin.app();
};
