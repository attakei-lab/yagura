/***************************************
 * Devel only features
 *
 * This module is used anytime, but called only in devel
 */
import * as express from 'express';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ipFilter = require('express-ipfilter');

interface AppOptions {
  root?: string;
  allowIps?: string[];
}

/**
 * Create Express server for static resource in devel
 */
export const createApp = (options: AppOptions): express.Express => {
  const app = express();
  if (options.allowIps) {
    console.log(options.allowIps);
    app.use(
      ipFilter.IpFilter(options.allowIps, { mode: 'allow', trustProxy: true }),
    );
  }
  if (options.root) {
    app.use(
      express.static(options.root, {
        extensions: ['html'],
      }),
    );
  }
  return app;
};
