/***************************************
 * Monitoring module
 */
import * as dayjs from 'dayjs';
import * as request from 'request-promise-native';

/**
 * Wait process
 * @param duration Sleep duration(milliseconds)
 */
const sleep = async (duration: number): Promise<void> => {
  return new Promise(resolve => {
    setTimeout(resolve, duration);
  });
};

/**
 * Monitoring target website
 *
 * @todo after, migrate core package
 */
export interface Site {
  /**
   * Name used in Yagura (not need same as site title)
   */
  name: string;
  /**
   * Target URL
   */
  url: string;
}

/**
 * Site monitoring controller
 */
export class SiteMonitor {
  /**
   * Process start datetime(used at report)
   */
  public readonly startAt: dayjs.Dayjs;
  /**
   * Monitoring target
   */
  public readonly site: Site;

  public constructor(site: Site) {
    this.startAt = dayjs();
    this.site = site;
  }

  /**
   * Request target
   * Retry if any invalid response
   *
   * @param retry Max retry nums
   */
  public async request(
    retry: number = 3,
    retryInterval: number = 3000,
  ): Promise<request.FullResponse> {
    const requestOptions: request.RequestPromiseOptions = {
      followRedirect: false,
      followAllRedirects: false,
      resolveWithFullResponse: true,
      strictSSL: false,
    };
    let resp;
    for (let i = 0; i < retry; i++) {
      // Any result must return (this method only request not work as inspector)
      resp = await request.get(this.site.url, requestOptions).then(
        resp => {
          return resp;
        },
        err => {
          return err;
        },
      );
      if (resp.statusCode && !String(resp.statusCode).startsWith('5')) {
        break;
      }
      await sleep(retryInterval);
    }
    return resp;
  }
}
