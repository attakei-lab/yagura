import { Response } from 'request';
import * as request from 'request-promise-native';
import { StatusCodeError } from 'request-promise-native/errors';
import * as t from './siteMonitor';

describe('SiteMonitor', () => {
  let monitor: t.SiteMonitor;
  let spyRequestGet: jest.SpyInstance;

  beforeEach(() => {
    monitor = new t.SiteMonitor({
      name: 'EXAMPLE.COM',
      url: 'http://example.com/',
    });
  });

  it('ok - real', async done => {
    const resp = await monitor.request();
    expect(resp.statusCode).toBe(200);
    done();
  });

  describe('error-retry', () => {
    spyRequestGet = jest.spyOn(request, 'get');

    beforeEach(() => {
      spyRequestGet.mockReset();
    });

    it('client-error(not-retry)', async done => {
      spyRequestGet.mockRejectedValue(
        new StatusCodeError(
          404,
          '',
          { url: 'http://example.com' },
          {} as Response,
        ),
      );
      await monitor.request();
      expect(spyRequestGet).toBeCalledTimes(1);
      done();
    });

    it('server-error(run-retry)', async done => {
      spyRequestGet.mockRejectedValue(
        new StatusCodeError(
          504,
          '',
          { url: 'http://example.com' },
          {} as Response,
        ),
      );
      await monitor.request(3, 0);
      expect(spyRequestGet).toBeCalledTimes(3);
      done();
    });
  });
});
