/***************************************
 * @attakei/yagura backend endpoint.
 *
 * This is main endpoint defined in package.json referred by Firebase Cloud Functions.
 * It does not written to import from other packages.
 * If you use theses modules in this package, import directly.
 */
import * as baseFunctions from 'firebase-functions';
import { configureBugsnag } from '@attakei/bugsnag-firebase';

import addSite from './backend/callables/addSite';
import configureUser from './backend/callables/configureUser';
import deleteSite from './backend/callables/deleteSite';
import * as monitorAll from './backend/topics/monitorAll';
import * as monitorSite from './backend/topics/monitorSite';
import { createApp } from './devel';
import { bugsnagConfig } from './config';


const functions = (
  bugsnagConfig.apiKey
  ?
  configureBugsnag(bugsnagConfig)
  :
  baseFunctions
);


export const ADD_SITE = functions.https.onCall(addSite);
export const DELETE_SITE = functions.https.onCall(deleteSite);
export const CONFIGURE_USER = functions.https.onCall(configureUser);

export const MONITOR_ALL = baseFunctions.pubsub
  .topic('monitor-all')
  .onPublish(monitorAll.handler);
export const MONITOR_SITE = baseFunctions.pubsub
  .topic('monitor-site')
  .onPublish(monitorSite.handler);

export const DEVEL_SERVER = functions.https.onRequest(
  createApp({
    root: `${__dirname}/www`,
    allowIps: baseFunctions.config().server.allowips
      ? baseFunctions.config().server.allowips.split(',')
      : [],
  }),
);
