/***************************************
 * Typedef collection for yagura-core/auth
 */

/**
 * User-profile managed in Firestore
 */
export interface UserProfile {
  /**
   * Email address (linked to Firebase Authentication)
   */
  email?: string;
  /**
   * User name (used in website)
   */
  name?: string;
  /**
   * Authorized to access admin-area
   */
  isAdmin: boolean;
}
