import * as functions from 'firebase-functions';
import * as auth from '..';
import { getApp, CallableFunction } from '../../firebase';

/**
 * callable function wrapper to except not authenticated context.
 *
 * @param func Target callable-function or other wrapper
 */
export const withAuthenticated = <D, R>(
  func: CallableFunction<D, R>,
): CallableFunction<D, R> => {
  return async (
    data: D,
    context: functions.https.CallableContext,
  ): Promise<R> => {
    // None authenticated(1)
    if (!context.auth) {
      throw new functions.https.HttpsError('unauthenticated', 'Pleas sign in');
    }
    return await func(data, context);
  };
};

/**
 * callable function wrapper to except user does not have admin role.
 *
 * @param func Target callable-function or other wrapper
 */
export const withOnlyAdmin = <D, R>(
  func: CallableFunction<D, R>,
): CallableFunction<D, R> => {
  return async (
    data: D,
    context: functions.https.CallableContext,
  ): Promise<R> => {
    const firestore = getApp().firestore();
    const UserProfileOr = await firestore
      .collection(auth.USER_PROFILE_COLLECTION)
      .doc(context.auth!.uid)
      .get()
      .then(ds => {
        if (!ds.exists) {
          return false;
        }
        return ds.data() as auth.UserProfile;
      })
      .catch(err => {
        throw new functions.https.HttpsError('unknown', err.message);
      });
    if (UserProfileOr === false || !UserProfileOr.isAdmin) {
      throw new functions.https.HttpsError('unknown', 'Administrator only');
    }
    return await func(data, context);
  };
};
