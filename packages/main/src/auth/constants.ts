/***************************************
 * Constants collection for yagura-core/auth
 */
import { UserProfile } from './types';

export const USER_PROFILE_COLLECTION = 'user_profiles';

export const ANONYMOUS_UID = 'anonymous';

export const ANONYMOUS_PROFILE: UserProfile = {
  isAdmin: false,
};
