/*******************
 * Configure authenticated user data
 */
import * as dayjs from 'dayjs';
import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as auth from '../../auth';

interface RequestData {
  email: string;
  name: string;
}

/**
 * User data managed in Firestore.
 * Keep properties not to have in Firebase Authentication.
 */
interface UserData {
  /**
   * User's email address to link Firebase Authentication
   */
  email: string;
  /**
   * User name used in website and notifications
   */
  name: string;
  /**
   * Flag to mean user privileged as admin
   */
  isAdmin: boolean;
}

export const handler = async (
  data: RequestData,
  context: functions.https.CallableContext,
): Promise<UserData> => {
  // None authenticated(1)
  if (!context.auth) {
    throw new functions.https.HttpsError('unauthenticated', 'Pleas sign in');
  }
  // None authenticated(2)
  admin.initializeApp();
  const userRecord = await admin
    .app()
    .auth()
    .getUser(context.auth.uid)
    .then(uc => uc)
    .catch(err => {
      throw new functions.https.HttpsError('not-found', err.message);
    });
  if (data.email !== userRecord.email) {
    throw new functions.https.HttpsError('permission-denied', 'Invalid email');
  }
  const userDataOr = await admin
    .app()
    .firestore()
    .doc(`${auth.USER_PROFILE_COLLECTION}/${context.auth.uid}`)
    .get()
    .then(ds => {
      if (ds.exists) {
        return ds.data() as UserData;
      }
      return false;
    })
    .catch(err => {
      throw new functions.https.HttpsError('unknown', err.message);
    });
  if (userDataOr !== false) {
    return userDataOr;
  }
  // Register user-data
  const userData = {
    isAdmin: false,
    ...data,
  };
  await admin
    .firestore()
    .doc(`${auth.USER_PROFILE_COLLECTION}/${context.auth.uid}`)
    .set(userData)
    .then(wr => {
      if (wr)
        console.debug(
          `Write user-data at ${dayjs(wr.writeTime.toDate()).toISOString()}`,
        );
    })
    .catch(err => {
      throw new functions.https.HttpsError('unknown', err.message);
    });
  return userData;
};

export default handler;
