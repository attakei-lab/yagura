/* eslint-disable @typescript-eslint/no-var-requires, @typescript-eslint/no-explicit-any */
import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

import * as auth from '../../auth';
import * as callable from './configureUser';

const firebaseMock = require('firebase-mock');

jest.spyOn(console, 'warn').mockImplementation(x => x);
jest.spyOn(console, 'log').mockImplementation(x => x);
jest.spyOn(console, 'debug').mockImplementation(x => x);

const spyAdminInitializeApp = jest.spyOn(admin, 'initializeApp');
const spyAdminApp = jest.spyOn(admin, 'app');

describe('handler()', () => {
  let mockFirebase: admin.app.App;

  beforeEach(() => {
    const mockAuth = new firebaseMock.MockFirebase();
    mockAuth.autoFlush();
    mockAuth.createUser({ email: 'test@example.com' });
    const mockFirestore = new firebaseMock.MockFirestore();
    mockFirestore.autoFlush();
    mockFirebase = firebaseMock.MockFirebaseSdk(
      null,
      () => mockAuth,
      () => mockFirestore,
    );
    spyAdminInitializeApp.mockImplementation(() => mockFirebase);
    spyAdminApp.mockImplementation(() => mockFirebase);
  });

  it('context.auth is not exists', async done => {
    callable
      .handler({} as any, {} as any)
      .then(u => expect(u).toBeUndefined())
      .catch(err => expect(err).toBeInstanceOf(functions.https.HttpsError))
      .finally(() => done());
  });

  it('User is not exists', async done => {
    callable
      .handler({} as any, { auth: { uid: 'xxxxx' } } as any)
      .then(u => expect(u).toBeUndefined())
      .catch(err => expect(err).toBeInstanceOf(functions.https.HttpsError))
      .finally(() => done());
  });

  it('User is invalid', async done => {
    const user = await admin
      .app()
      .auth()
      .getUserByEmail('test@example.com');
    await callable
      .handler(
        { email: 'test2@example.com' } as any,
        { auth: { uid: user.uid } } as any,
      )
      .then(u => expect(u).toBeUndefined())
      .catch(err => expect(err).toBeInstanceOf(functions.https.HttpsError))
      .finally(() => done());
  });

  it('UserData created', async done => {
    const user = await admin
      .app()
      .auth()
      .getUserByEmail('test@example.com');
    await callable
      .handler(
        { email: 'test@example.com', name: 'Test user' } as any,
        { auth: { uid: user.uid } } as any,
      )
      .then(u => {
        expect(u).toHaveProperty('email');
        expect(u).toHaveProperty('name');
        expect(u.isAdmin).toBeFalsy();
      });
    await admin
      .app()
      .firestore()
      .collection(auth.USER_PROFILE_COLLECTION)
      .get()
      .then(qs => {
        expect(qs.empty).toBeFalsy();
      });
    done();
  });

  it('UserData is already exists', async done => {
    const user = await admin
      .app()
      .auth()
      .getUserByEmail('test@example.com');
    await admin
      .app()
      .firestore()
      .collection(auth.USER_PROFILE_COLLECTION)
      .doc(user.uid)
      .set({ email: 'test@example.com', name: 'test user', isAdmin: false });
    await callable
      .handler(
        { email: 'test@example.com', name: 'Test user' } as any,
        { auth: { uid: user.uid } } as any,
      )
      .then(u => {
        expect(u).toHaveProperty('email');
        expect(u).toHaveProperty('name');
        expect(u.isAdmin).toBeFalsy();
      });
    await admin
      .app()
      .firestore()
      .collection(auth.USER_PROFILE_COLLECTION)
      .get()
      .then(qs => {
        expect(qs.size).toBe(1);
      });
    done();
  });
});
