/*******************
 * Add site by Cloud Functions
 */
import * as functions from 'firebase-functions';
import { withAuthenticated } from '../../auth/services/wrappers';
import { getApp } from '../../firebase';
import * as sites from '../../sites';

type RequestData = sites.SiteData;

export const handler = async (
  data: RequestData,
  context: functions.https.CallableContext,
): Promise<string|null> => {
  const firestore = getApp().firestore();
  const register = new sites.services.SiteRegister(data, firestore);
  if (await register.isUrlExists()) {
    throw new functions.https.HttpsError('already-exists', 'Same URL website is already exists.');
  }
  return await register.process();
};

export default withAuthenticated(handler);
