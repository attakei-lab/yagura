/*******************
 * Configure authenticated user data
 */
import * as functions from 'firebase-functions';
import { withAuthenticated, withOnlyAdmin } from '../../auth/services/wrappers';
import { getApp } from '../../firebase';
import * as sites from '../../sites';

interface RequestData {
  siteId: string;
}

export const handler = async (
  data: RequestData,
  context: functions.https.CallableContext,
): Promise<void> => {
  const firestore = getApp().firestore();
  const siteSnapshot = await firestore
    .collection(sites.SITE_COLLECTION)
    .doc(data.siteId)
    .get()
    .then(ds => {
      if (!ds.exists) {
        return null;
      }
      return ds;
    });
  if (siteSnapshot === null) {
    throw new functions.https.HttpsError('not-found', 'Site is not found.');
  }
  return await siteSnapshot.ref.delete().then(() => {
    return;
  });
};

export default withAuthenticated(withOnlyAdmin(handler));
