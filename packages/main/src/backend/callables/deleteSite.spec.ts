/* eslint-disable @typescript-eslint/no-var-requires, @typescript-eslint/no-explicit-any */
import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

import { getApp } from '../../firebase';
import * as callable from './deleteSite';

const MockFirebase = require('mock-cloud-firestore');

jest.spyOn(console, 'warn').mockImplementation(x => x);
jest.spyOn(console, 'log').mockImplementation(x => x);
jest.spyOn(console, 'debug').mockImplementation(x => x);

jest.mock('../../firebase');

describe('handler()', () => {
  let mockFirebase: admin.app.App;

  beforeEach(() => {
    mockFirebase = new MockFirebase({
      __collection__: {
        sites: {
          __doc__: {
            example: {
              name: 'EXAMPLE.COM',
              url: 'http://example.com',
            },
          },
        },
      },
    });
    (getApp as any).mockReturnValue(mockFirebase);
  });

  it('site is not exists', async done => {
    callable
      .handler({ siteId: 'xxx' }, {} as any)
      .then(() => fail('Does not return as regular'))
      .catch(err => {
        expect(err).toBeInstanceOf(functions.https.HttpsError);
      })
      .finally(() => done());
  });

  it('exists', async done => {
    callable.handler({ siteId: 'example' }, {} as any).then(
      async () => {
        await mockFirebase
          .firestore()
          .collection('sites')
          .get()
          .then(qs => expect(qs.docs).toHaveLength(0));
        done();
      },
      err => fail(err),
    );
  });
});
