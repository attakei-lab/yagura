/***************************************
 *
 */
import * as functions from 'firebase-functions';
import { getApp } from '../../firebase';
import { Site, SiteMonitor } from '../../monitors/services/siteMonitor';

/**
 * Monitor site and inspect
 *
 * @param message Emitted site-id
 * @param context (not used)
 */
export const handler = async (
  message: functions.pubsub.Message,
): Promise<void> => {
  console.log('Start monitoring');
  const siteId = message.json.id;
  const firestore = getApp().firestore();
  const siteRef = firestore.collection('sites').doc(siteId);
  const site = await siteRef.get().then(ds => {
    if (!ds.exists) {
      return null;
    }
    return ds.data() as Site;
  });
  if (site === null) {
    console.warn('Site is not exits in DB.');
    return;
  }
  const monitor = new SiteMonitor(site);
  const resp = await monitor.request();
  console.log(`Monitor date ${monitor.startAt}`);
  console.log(`HTTP Status: ${resp.statusCode}`);
  const reportRef = siteRef
    .collection('reports')
    .doc(monitor.startAt.unix().toString());
  await reportRef.set({
    monitorAt: monitor.startAt.unix(),
    httpStatus: {
      state: resp.statusCode && resp.statusCode === 200 ? 'OK' : 'NG',
      excepted: 200,
      actually: resp.statusCode,
      message: '',
    },
  });
};
