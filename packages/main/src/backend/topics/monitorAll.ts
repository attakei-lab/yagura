/***************************************
 *
 */
import { PubSub } from '@google-cloud/pubsub';
import * as admin from 'firebase-admin';

/**
 * Emit monitoring trigger per all sites
 *
 * @param message (not used)
 * @param context (not used)
 */
export const handler = async (): Promise<void> => {
  console.log('Start monitoring');
  admin.initializeApp();
  const app = admin.app();
  const pubSub = new PubSub({
    projectId: app.options.projectId,
  });
  const firesore = app.firestore();
  await firesore
    .collection('sites')
    .get()
    .then(qs => {
      return qs.docs.map(async ds => {
        const message = {
          id: ds.id,
        };
        await pubSub.topic('monitor-site').publishJSON(message);
      });
    });
};
