/***************************************
 * CLI command endpoint
 */
import * as path from 'path';
import * as yargs from 'yargs';

const commandsDir = path.join(__dirname, 'cli');

yargs
  .commandDir(commandsDir, {
    extensions: ['js', 'ts'],
    // Ignore jest source (in same path)
    exclude: /.+\.spec\.ts/,
  })
  .demandCommand().argv;
