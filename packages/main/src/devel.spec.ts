import * as express from 'express';
import * as path from 'path';
import * as request from 'supertest';

import * as t from './devel';

describe('createServer()', () => {
  it('Not configure', () => {
    const app = t.createApp({});
    expect(app).toBeTruthy();
  });

  describe('devel-behavior', () => {
    let app: express.Express;

    beforeEach(() => {
      app = t.createApp({
        root: path.join(__dirname, '..', 'test', 'devel-server'),
      });
    });

    it('index', async done => {
      request(app)
        .get('/')
        .then(resp => {
          expect(resp.status).toBe(200);
          done();
        });
    });

    it('none ext', async done => {
      request(app)
        .get('/dashboard')
        .then(resp => {
          expect(resp.status).toBe(200);
          done();
        });
    });
  });
});
