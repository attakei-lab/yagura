/***************************************
 * Constants collection for yagura/sites
 */

/**
 * Collection name for sites in Cloud Firestore
 */
export const SITE_COLLECTION = 'sites';
