/***************************************
 * Site registration pure component
 */
import { firestore } from 'firebase-admin';

import { SITE_COLLECTION } from '../constants';
import { SiteData } from "../types";

/**
 * Site registration operator
 */
export class SiteRegister {
  /**
   * Registeration candicate
   */
  public readonly siteData: SiteData;
  /**
   * Registration firestore database
   */
  public readonly firestore: firestore.Firestore
  
  public constructor(siteData: SiteData, firestore: firestore.Firestore) {
    this.siteData = siteData;
    this.firestore = firestore;
  }

  /**
   * Check be already exists same url site-data in Firestore
   */
  public async isUrlExists(): Promise<boolean> {
    return await this.firestore.collection(SITE_COLLECTION)
      .where('url', '==', this.siteData.url)
      .get()
      .then(qs => !qs.empty);
  }

  /**
   * Register site-data into Firestore with binding default values
   */
  public async process(): Promise<string|null> {
    const siteRef = this.firestore.collection(SITE_COLLECTION).doc();
    await siteRef.set(this.siteData).then(wr => {
      if (wr) {
        console.debug(`Write new site-data at ${wr.writeTime.toDate()}`);
      }
    });
    return siteRef.id;
  }
}
