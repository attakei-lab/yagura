import { app } from 'firebase-admin';
import * as t from './siteRegister';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const MockFirebase = require('mock-cloud-firestore');

jest.spyOn(console, 'warn').mockImplementation(x => x);
jest.spyOn(console, 'log').mockImplementation(x => x);
jest.spyOn(console, 'debug').mockImplementation(x => x);
jest.mock('../../firebase');

describe('SiteRegister', () => {
  let mockFirebase: app.App;

  beforeEach(() => {
    mockFirebase = new MockFirebase({
      __collection__: {
        sites: {
          __doc__: {
            example: {
              name: 'EXAMPLE.COM',
              url: 'http://example.com',
            },
          },
        },
      },
    });
  });

  describe('isUrlExists()', () => {
    it('Not exists', async (done) => {
      const reg = new t.SiteRegister(
        {name: 't', url: 'http://example.org'},
        mockFirebase.firestore()
      );
      expect(await reg.isUrlExists()).toBeFalsy();
      done();
    });

    it('Exists', async (done) => {
      const reg = new t.SiteRegister(
        {name: 't', url: 'http://example.com'},
        mockFirebase.firestore()
      );
      expect(await reg.isUrlExists()).toBeTruthy();
      done();
    });
  });

  describe('process()', () => {
    it ('ok', async (done) => {
      const reg = new t.SiteRegister(
        {name: 't', url: 'http://example.org'},
        mockFirebase.firestore()
      );
      await reg.process();
      const colSize = await mockFirebase.firestore().collection('sites').get().then(qs => qs.docs.length);
      expect(colSize).toBe(2);
      done();
    });
  });
});
