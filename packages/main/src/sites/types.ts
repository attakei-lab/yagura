/***************************************
 * Typedef collection for yagura-core/sites
 */

/**
 * Basic information of monitoring target website
 */
export interface SiteData {
  /**
   * Name used in Yagura (not need same as site title)
   */
  name: string;
  /**
   * Target URL
   */
  url: string;
}
