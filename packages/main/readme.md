# @attakei/yagura

Yagura main components.

## Description

This sub-package manage these.

* Backend scripts running in Cloud Functions.
* CLI handler to access Firebase backends. 
* Shared types and logics for client application.


## Module structures

* `index`: Endpoint for Cloud Functions.
* `backend/`: Backend operations by Cloud Functions.
* `cli/`: CLI endpoint and subcommands.
* `(any)/`: Domain logic models.
    * `(any)/constants`: Const values.
    * `(any)/types`: Type definitions.
    * `(service)/services/`: Logics subpackage.
* `www/`: Static resources for devel. (access controlled)
