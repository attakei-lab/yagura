#  -------------------------------------
#  Command combnation wrapper
#  -------------------------------------
.PHONY: version

#  -----------------
#  Variables
#  -----------------
VERSION ?= '0.1.0'
ifdef services
DEPLOY_TARGET = --only=$(services)
endif

#  -----------------
#  Targets
#  -----------------
# Set version to all subpackages
versions:
	yarn version --no-git-tag-version --new-version $(VERSION)
	yarn workspaces run version --no-git-tag-version --new-version $(VERSION)

# Deploy to Firebase
deploy-prod: build-all predeploy-hosting
	yarn copyfiles -u 3 'packages/www/out/**/*' public/
	yarn firebase deploy $(DEPLOY_TARGET)

# deploy-devel: build-all predeploy-hosting
deploy-devel: build-all predeploy-hosting
	yarn copyfiles -u 3 'packages/www/out/**/*' packages/main/lib/www/
	yarn firebase deploy $(DEPLOY_TARGET)

# Build any projects
build-all: build-main build-www

build-main:
	yarn workspace @attakei/yagura build

build-www: build-main
	yarn workspace @attakei/yagura-www build
	yarn workspace @attakei/yagura-www export

# Pre-deploy for Firebase Hosting
predeploy-hosting:
	yarn rimraf public
	yarn copyfiles -u 2 'firebase/hosting/**/*' public/
